import { Product } from './product';

export const PRODUCTS: Product[] = [
	{
		id: 1,
		title: 'Butterfly',
		price: 39,
		thumbnail: 'butterfly.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 2,
		title: 'Denim',
		price: 49,
		thumbnail: 'denim.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 3,
		title: 'Green',
		price: 29,
		thumbnail: 'green.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 4,
		title: 'Grey',
		price: 24,
		thumbnail: 'grey.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 5,
		title: 'Lightblue',
		price: 34,
		thumbnail: 'lightblue.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 6,
		title: 'Pattern',
		price: 59,
		thumbnail: 'pattern.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 7,
		title: 'Sand',
		price: 19,
		thumbnail: 'sand.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	},
	{
		id: 8,
		title: 'White',
		price: 14,
		thumbnail: 'white.png',
		images: [
			'https://dummyimage.com/200x200/eee/fff.jpg&text=+',
			'https://dummyimage.com/200x200/333/fff.jpg&text=+',
			'https://dummyimage.com/200x200/999/fff.jpg&text=+'
		]
	}
]