import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Product }        from './../product';
import { ProductService } from './../shared/service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  banner: object = {
    title: '50% off sommer collection',
    background: 'product-banner.jpg'
  };

	product: Product;

  constructor(
  	private productService: ProductService,
  	private route: ActivatedRoute,
  	private location: Location
  ) { }

  ngOnInit(): void {
  	this.route.paramMap
  		.switchMap((params: ParamMap) => this.productService.getProduct(+params.get('id')))
  		.subscribe(product => this.product = product);
  
  }

  test(item): void {
    console.log(item);
  }

}
