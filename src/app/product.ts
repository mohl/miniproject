export class Product {
	id:        number;
	title:     string;
	price:     number;
	thumbnail: string;
	images:    string[];
}
