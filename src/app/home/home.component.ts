import { Component, OnInit } from '@angular/core';

import { Product }        from './../product';
import { ProductService } from './../shared/service/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  slides: any[] = [
    {
      title: 'Some title',
      image: 'https://dummyimage.com/200x200/eee/fff.jpg&text=+'
    },
    {
      title: 'Some other title',
      image: 'https://dummyimage.com/200x200/eee/fff.jpg&text=+'
    }
  ];

	products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
  	this.getProducts();
  }

  getProducts() {
  	this.productService.getProducts().then(products => this.products = products.slice(0,4));
  }

}
