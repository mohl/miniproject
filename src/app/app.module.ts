import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule }     from './shared/shared.module';
import { HomeModule }       from './home/home.module';
import { ProductModule }    from './product/product.module';
import { ContactModule }    from './contact/contact.module';
import { CollectionModule } from './collection/collection.module';

import { ProductService } from './shared/service/product.service';

import { AppComponent } from './app.component';
import { CollectionComponent } from './collection/collection.component';

@NgModule({
  declarations: [
    AppComponent,
    CollectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    
    HomeModule,
    ProductModule,
    ContactModule
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
