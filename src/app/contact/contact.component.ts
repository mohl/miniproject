import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

	rForm: FormGroup;
	post:any;
	description: string = '';
	name:string = '';
	valid:boolean = false;

	alert:object = {
		name: 'A name is required',
		description: 'You must specify a description that\'s between 30 and 500 characters'
	};

  constructor(private fb: FormBuilder) {
  	this.rForm = fb.group({
  		'name': [null, Validators.required],
  		'description': [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(500)])]
  	});
  }

  onSubmit(post): void {
  	this.description = post.description;
  	this.name = post.name;
  	this.valid = true;
  }

  ngOnInit() {
  }

}
