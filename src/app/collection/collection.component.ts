import { Component, OnInit } from '@angular/core';

import { Product }        from './../product';
import { ProductService } from './../shared/service/product.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

	products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
  	this.getProducts();
  }
  
  getProducts() {
  	this.productService.getProducts().then(products => this.products = products);
  }

}
