import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
//import { RouterLink } from '@angular/router';

import { HeaderComponent } from './layout/header.component';
import { FooterComponent } from './layout/footer.component';
import { SlideComponent } from './slide/slide.component';
import { HeroComponent } from './hero/hero.component';
import { BannerComponent } from './banner/banner.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [HeaderComponent, FooterComponent, SlideComponent, HeroComponent, BannerComponent],
  exports: [HeaderComponent, FooterComponent, SlideComponent, HeroComponent, BannerComponent]
})
export class SharedModule { }
