import { Injectable } from '@angular/core';

import { Product } from './../../product';
import { PRODUCTS } from './../../product-mock';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductService {

  constructor() { }

  getProducts(): Promise<Product[]> {
  	return Promise.resolve(PRODUCTS);
  }

	getProduct(id: number): Promise<Product> {
		return this.getProducts()
			.then(products => products.find(product => product.id === id));
	}

}