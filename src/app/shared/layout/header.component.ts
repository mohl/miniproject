import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	paths: any[] = [
		'/',
		'/collection'
	];

	headerClass: boolean = false;

  constructor(private location: Location) { }

  ngOnInit() {
  	console.log(this.paths);
  	console.log(location)
  	this.checkLocation();
  }

  checkLocation() {
  	if ( this.paths.includes(location.pathname) ) 
  		this.headerClass = true;
  }

}
