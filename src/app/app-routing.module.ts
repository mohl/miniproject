import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }      from './home/home.component';
import { ProductComponent }   from './product/product.component';
import { CollectionComponent} from './collection/collection.component';
import { ContactComponent }   from './contact/contact.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product/:id', component: ProductComponent },
  { 
  	path: 'collection', 
  	component: CollectionComponent
  },
  { path: 'contact', component: ContactComponent }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}